# TRC Scrubber #

Micromed TRC files recorded in hospitals usually contain patient names and dates of birth.
In case such files are to be shared with researchers outside the hospital, personally identifying information has to be removed in order to ensure patient privacy.

## Installation ##
Requirements: Tested with Python 3.6.3. Follow installation instructions for Python for your operating system.

## Usage ##
For usage information, run the script on the command line, as `trc_scrubber.py -h`.

## Implementation ##

The file format was deduced from data readers in [FieldTrip](https://github.com/fieldtrip/fieldtrip/blob/master/fileio/private/read_micromed_trc.m) and (for DOB) from inspection of the raw file.

Name and DOB fields can be replaced with custom information. All efforts have been made to prevent file corruption. E.g. name strings have a hard length limit. However, we cannot take any responsibility for damage or loss of data that may occur in conjunction with usage of this script.


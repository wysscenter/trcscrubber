#!/usr/bin/env python3
"""
TRC Scrubber is a tool to remove name and date of birth fields from Micromed TRC files.
Author: Jonas B Zimmermann
The Wyss Center for Bio and Neuroengineering
Geneva Switzerland
(c) 2018


Date: 22 Jan 2018
"""
import argparse
import glob

MICROMED_FILE_HEAD = b'* MICROMED  Brain-Quick file *'

class Error(Exception):
    """Base class for exceptions in this module."""
    pass

class NotAMicroMedFileError(Error):
    """Exception raised if trying to modify a non-Mircomed file.

    Attributes:
        file_name -- name of the offending file
        message -- explanation of the error
    """

    def __init__(self, file_name, message="Not a Micromed File"):
        self.file_name = file_name
        self.message = message


def overwriteNameFields(file_name, surname="", name="", dob=0, mob=0, yob=0, verbose=0):
    with open(file_name, "r+b") as fh:
        if verbose > 0:
            print("- {} ... ".format(file_name), end='')
        file_type = fh.read(30)
        if file_type != MICROMED_FILE_HEAD:
            raise NotAMicroMedFileError(file_name)
        surname_b = bytearray(surname, 'ascii')
        name_b = bytearray(name, 'ascii')
        surname_b = surname_b.ljust(22, b"\x20")[:22]
        name_b = name_b.ljust(20, b"\x20")[:20]
        dob_b = bytes([mob & 0xff, dob & 0xff, yob & 0xff])
        fh.seek(64)
        fh.write(surname_b)
        fh.write(name_b)
        fh.seek(106)
        fh.write(dob_b)
        if verbose > 0:
            print("done.")
        


if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description="TRC Scrubber is a tool to remove name and date of birth fields from Micromed TRC files. Version 1.0""", epilog="Author: Jonas B Zimmermann - The Wyss Center for Bio and Neuroengineering - Geneva, Switzerland (c) 2018")
    parser.add_argument('-s', '--surname', default="", help="New surname to write to file (truncated to 22 Bytes)")
    parser.add_argument('-n', '--firstname', default="", help="New first name to write to file (truncated to 20 Bytes)")
    parser.add_argument('-d', '--day-of-birth', default=1, type=int, help="New day of the month of DOB")
    parser.add_argument('-m', '--month-of-birth', default=1, type=int, help="New month of DOB")
    parser.add_argument('-y', '--year-of-birth', default=0, type=int, help="New year of DOB (enter year - 1900)")
    parser.add_argument('-v', '--verbose', default=0, action='count', help="Show more information during processing")
    parser.add_argument('filenames', metavar='recording.trc', type=str, nargs='+',
                    help='File name of a Micromed TRC file. Unix-style file name globbing is supported. For example: *.trc captures all files ending in .trc in the current directory.')
    args = parser.parse_args()
    converted_files = 0

    for files in([glob.glob(arg) for arg in args.filenames]):
        for fn in files:
            try:
                overwriteNameFields(fn, surname=args.surname, name=args.firstname, dob=args.day_of_birth, mob=args.month_of_birth, yob=args.year_of_birth, verbose=args.verbose)
                converted_files += 1
            except FileNotFoundError:
                print("File '{}' not found.".format(fn))
            except NotAMicroMedFileError as err:
                print("{}: {}".format(err.message, err.file_name))
    print("Successfully changed headers of {} Micromed files.".format(converted_files))
